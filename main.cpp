// Adaptive++
// Copyright (C) 2017  João Pedro de O. Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <tuple>
#include <array>

//TEST 1 - constructing fixed number objects inside a template class, which are of a template arguments classes

class A {
public:
  int x;
   A(int x) : x(x){};
};

class B {
public:
  float x, y;
  B(float x, float y) : x(x), y(y) {};
};

template<class C1, class C2>
struct Y {
public:
  template<typename... U, typename... V>
  Y(std::tuple<U...> u, std::tuple<V...> v)
    : a(std::make_from_tuple<C1>(std::move(u))),
      b(std::make_from_tuple<C2>(std::move(v)))
  {}

  C1 a;
  C2 b;
};

//TEST 2 - constructing objects inside an array, inside a template class, which are of a template arguments classesw

template <typename T, size_t N>
struct Wrapper {
  template <typename... Args>
  Wrapper(int b_in, Args&&... args) : b(b_in), a{std::make_from_tuple<T>(std::move(args))...} {}
  int b;
  std::array<T, N> a;
};

//TEST 3 - constructing variadic objects, inside a template class, which are of a template arguments classesw

template <typename... Ts>
struct Test3 {
  template <typename... Args>
  Test3(int b_in, Args&&... args) : b(b_in), a{std::make_from_tuple<Ts>(std::move(args))...} {}
  int b;
  std::tuple<Ts...> a;
};

int main() {
  //test 1
  Y<A, B> y(std::make_tuple(1), std::make_tuple(1.0f, 2.0f));
  //test 2
  std::array<A, 2> a{12, 34};
  Wrapper<A, 3> w1(3, std::make_tuple(12), std::make_tuple(34), std::make_tuple(19));
  Wrapper<B, 2> w2(3, std::make_tuple(12.0f, 34.0f), std::make_tuple(19.0f, 22.0f));
  Test3<A, B, A> t1(3, std::make_tuple(12), std::make_tuple(12.0f, 34.0f), std::make_tuple(19));
  return 0;
}
